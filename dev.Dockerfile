FROM node:11

ENV NODE_ENV development

WORKDIR /app

CMD bash -c "npm i && npm run tsc:watch"
