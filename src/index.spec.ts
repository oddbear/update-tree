import { updateTree } from './index';

describe('File "object.ts"', () => {
	describe('function updateTree', () => {
		it('returns oldState if oldState and newState are equal', () => {
			const oldState = { foo: 'bar' };
			const newState = { ...oldState };

			expect(updateTree(oldState, newState)).toStrictEqual(oldState);
		});

		it('returns newState if oldState and newState are different', () => {
			const oldState = { foo: 'bar' };
			const newState = { foo: 'zaz' };

			expect(updateTree(oldState, newState)).toStrictEqual(newState);
		});

		it('returns newState if oldState has more fields than newState', () => {
			const oldState = { foo: 'bar' };
			const newState = {};

			expect(updateTree(oldState, newState)).toStrictEqual(newState);
		});

		it('returns newState if oldState has less fields than newState', () => {
			const oldState = {};
			const newState = { foo: 'bar' };

			expect(updateTree(oldState, newState)).toStrictEqual(newState);
		});

		it('returns new state with a same properties of oldState if deep equal pass', () => {
			const oldState = { foo: { deep: { equal: { very: { deep: 2 } } } } };
			const newState = { foo: { deep: { equal: { very: { deep: 2 } } } } };

			expect(updateTree(oldState, newState).foo).toStrictEqual(oldState.foo);
		});

		it('returns new state with a same properties of newState if deep equal failed', () => {
			const oldState = { foo: { deep: { equal: { very: { deep: 2 } } } } };
			const newState = { foo: { deep: { equal: { very: { deep: 3 } } } } };

			expect(updateTree(oldState, newState).foo).not.toStrictEqual(oldState.foo);
		});

		it('returns new state with a mixed properties based on deep equality', () => {
			const oldState = {
				take: 1,
				states: ['idle', 'work'],
				users: {
					guests: ['John'],
				},
				rates: {
					youtube: 0.6,
					netflix: 'tbd',
				},
			};
			const newState = {
				take: 34,
				states: ['idle', 'work'],
				users: {
					guests: ['John'],
					admins: ['Randy', 'Elizabet'],
				},
				rates: {
					youtube: 0.6,
					netflix: 'tbd',
				},
			};

			const tree = updateTree(oldState, newState);

			// save oldState
			expect(tree.states).toStrictEqual(oldState.states);
			expect(tree.rates).toStrictEqual(oldState.rates);

			// save newState
			expect(tree.take).toStrictEqual(newState.take);
			expect(tree.users).toStrictEqual(newState.users);
		});

		it('(levelTree: >0) returns new state with different object-levels when they are the same', () => {
			const oldState = {
				foo: {
					bar: {
						zer: {
							obj: 1,
						},
						counter: 1,
					},
					baz: 2,
				},
			};
			const newState = {
				foo: {
					bar: {
						zer: {
							obj: 1,
						},
						counter: 2,
					},
					baz: 4,
				},
			};

			const tree = updateTree(oldState, newState, {
				levelTree: 1,
			});

			// shake 1st level-tree
			expect(tree.foo).not.toBe(oldState.foo);
			expect(tree.foo).not.toBe(newState.foo);

			// shake 2nd level-tree
			expect(tree.foo.bar).not.toBe(oldState.foo.bar);
			expect(tree.foo.bar).toBe(newState.foo.bar);

			// just check on any reason
			expect(tree.foo.bar).not.toBe(oldState.foo.baz);
			expect(tree.foo.bar).not.toBe(newState.foo.baz);
		});
	});
});
