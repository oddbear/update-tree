import deepEqual from 'fast-deep-equal';

export const updateTree = <O extends object, N extends object>(
	oldState: O,
	newState: N,
	options?: { levelTree?: number },
): N => {
	const levelTree = options?.levelTree ?? 0;

	const allKeys = new Set(Object.keys(oldState).concat(Object.keys(newState)));
	const resultState: Partial<N> = {};

	// if oldState wasn't updated then return oldState to prevent unnecessary mutations
	let isUpdated = false;

	for (const key of allKeys) {
		// it means that a property deleted
		if (!(key in newState)) {
			isUpdated = true;
			continue;
		}

		// time economy
		const oldStateProperty = oldState[key as keyof O];
		const newStateProperty = newState[key as keyof N];

		// if newValue already has property than oldState
		if (!(key in oldState)) {
			isUpdated = true;
			resultState[key as keyof N] = newStateProperty;
			continue;
		}

		// typescript logic isn't perfect so this magic with types is required
		// just deep equal properties
		if (deepEqual(oldStateProperty, newStateProperty)) {
			resultState[key as keyof N] = (<unknown>oldStateProperty) as N[keyof N];
			continue;
		}

		// recursion's endpoint
		if (levelTree == 0) {
			isUpdated = true;
			resultState[key as keyof N] = newStateProperty;
			continue;
		}

		// last variant when properties have different structures
		if (typeof oldStateProperty === 'object' && typeof newStateProperty === 'object') {
			// again really hard work with typescript, lets increase readability
			// @ts-ignore
			const resultTree: N[keyof N] = updateTree(oldStateProperty, newStateProperty, levelTree - 1);
			// @ts-ignore
			if (resultTree === oldStateProperty) {
				continue;
			}

			isUpdated = true;
			resultState[key as keyof N] = resultTree;
			continue;
		}

		isUpdated = true;
		// primitives
		resultState[key as keyof N] = newStateProperty;
	}

	return isUpdated ? (resultState as N) : ((<unknown>oldState) as N);
};
